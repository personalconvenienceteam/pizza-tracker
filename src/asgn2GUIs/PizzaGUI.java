package asgn2GUIs;

import java.awt.event.ActionEvent;


import java.awt.event.ActionListener;
import java.io.File;
import java.text.NumberFormat;

import asgn2Customers.Customer;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Restaurant.PizzaRestaurant;

import java.awt.*;
import javax.swing.*;


/**
 * This class is the graphical user interface for the rest of the system. 
 * Currently it is a �dummy� class which extends JFrame and implements Runnable and ActionLister. 
 * It should contain an instance of an asgn2Restaurant.PizzaRestaurant object which you can use to 
 * interact with the rest of the system. You may choose to implement this class as you like, including changing 
 * its class signature � as long as it  maintains its core responsibility of acting as a GUI for the rest of the system. 
 * You can also use this class and asgn2Wizards.PizzaWizard to test your system as a whole
 * 
 * 
 * @author Roderick Lenz
 *
 */
public class PizzaGUI extends javax.swing.JFrame implements Runnable {

    /**
     * 
     */
    private static final long serialVersionUID = 4801238894227071688L;
    private static final int WIDTH = 1500;
    private static final int HEIGHT = 927;
    private static final JFileChooser fileChooser = new JFileChooser();
    private static final String STARTUP_TEXT = "Please load a log file to begin.\n\n\n";

    private PizzaRestaurant restaurant;

    private static JPanel titlePanel;
    private static JPanel textPanel;
    private static JPanel buttonPanel;
    private static JPanel tablePanel;

    private static JButton openLogButton;
    private static JButton displayLogButton;
    private static JButton resetButton;
    private static JButton getProfitButton;
    private static JButton getDistanceButton;

    private static JTextArea startupTextArea;

    private static JScrollPane tableScrollPane;

    /**
     * Creates a new Pizza GUI with the specified title 
     * @param title - The title for the supertype JFrame
     */
    public PizzaGUI(String title) {
        super(title);
        setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        titlePanel = new JPanel(new BorderLayout());

        JLabel label = new JLabel(title);
        titlePanel.add(label, BorderLayout.CENTER);
        add(titlePanel, BorderLayout.NORTH);

        textPanel = new JPanel();
        textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.PAGE_AXIS));
        textPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        startupTextArea = new JTextArea();
        startupTextArea.setLineWrap(true);
        startupTextArea.setWrapStyleWord(true);
        startupTextArea.append(STARTUP_TEXT);
        startupTextArea.setEditable(false);
        startupTextArea.setMaximumSize(new Dimension(WIDTH, 50));
        textPanel.add(startupTextArea);

        tablePanel = new JPanel();
        tablePanel.setLayout(new BoxLayout(tablePanel, BoxLayout.PAGE_AXIS));
        tablePanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        tableScrollPane = new JScrollPane();
        tableScrollPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        textPanel.add(tableScrollPane);

        add(textPanel, BorderLayout.CENTER);


        buttonPanel = new JPanel();
        openLogButton = new JButton("Open Log File");
        openLogButton.addActionListener(new ButtonListener());
        buttonPanel.add(openLogButton);

        displayLogButton = new JButton("Display Log");
        displayLogButton.addActionListener(new ButtonListener());
        displayLogButton.setEnabled(false);
        buttonPanel.add(displayLogButton);

        getProfitButton = new JButton("Calculate Profit");
        getProfitButton.addActionListener(new ButtonListener());
        getProfitButton.setEnabled(false);
        buttonPanel.add(getProfitButton);

        getDistanceButton = new JButton("Calculate Distance");
        getDistanceButton.addActionListener(new ButtonListener());
        getDistanceButton.setEnabled(false);
        buttonPanel.add(getDistanceButton);

        resetButton = new JButton("Reset View");
        resetButton.addActionListener(new ButtonListener());
        resetButton.setEnabled(false);
        buttonPanel.add(resetButton);

        add(buttonPanel, BorderLayout.PAGE_END);

        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        pack();
        setVisible(true);
    }


    @Override
    public void run() {
        // TO DO
    }

    private void importLogFile(){
        boolean logProcessed = false;
        File file;
        int returnVal = fileChooser.showOpenDialog(PizzaGUI.this);
        String fileName = "";

        if (returnVal == JFileChooser.APPROVE_OPTION){
            file = fileChooser.getSelectedFile();
            restaurant = new PizzaRestaurant();
            try {
                fileName = file.getAbsolutePath();
                logProcessed = restaurant.processLog(fileName);
            } catch (CustomerException e) {
                JOptionPane.showMessageDialog(PizzaGUI.this, e.getMessage());
                e.printStackTrace();
            } catch (PizzaException e) {
                JOptionPane.showMessageDialog(PizzaGUI.this, e.getMessage());
                e.printStackTrace();
            } catch (LogHandlerException e) {
                JOptionPane.showMessageDialog(PizzaGUI.this, e.getMessage());
                e.printStackTrace();
            }
        }

        if (logProcessed){
            PizzaGUI.openLogButton.setEnabled(false);
            PizzaGUI.displayLogButton.setEnabled(true);
            PizzaGUI.startupTextArea.selectAll();
            PizzaGUI.startupTextArea.replaceSelection("Log file loaded successfully.\n" + 
                    "File name: " + fileName + "\n"+
                    "Press the Display Log button to continue...\n");
        }

    }


    private void displayLog(){
        int numberOfOrders = restaurant.getNumCustomerOrders();
        String[] columnNames = {"Customer Name",
                "Mobile Number",
                "Customer Type",
                "Location (x,y)",
                "Delivery Distance",
                "Pizza Type",
                "Quantity",
                "Order Price",
                "Order Cost",
        "Order Profit"};

        String[][] orders = new String[numberOfOrders][10];

        for (int i=0; i<numberOfOrders; i++){
            try{
                NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();

                Pizza pizza = restaurant.getPizzaByIndex(i);	            
                Customer customer = restaurant.getCustomerByIndex(i);

                String customerName = customer.getName();
                String mobileNumber = customer.getMobileNumber();
                String customerType = customer.getCustomerType();
                String locationXY = String.format("( %d , %d )", customer.getLocationX(), customer.getLocationY());
                String distance = String.format("%.2f Blocks", customer.getDeliveryDistance());
                String pizzaType = pizza.getPizzaType();
                String pizzaQuantity = String.format("%d", pizza.getQuantity());
                String orderPrice = currencyFormatter.format(pizza.getOrderPrice());
                String orderCost = currencyFormatter.format(pizza.getOrderCost());
                String orderProfit = currencyFormatter.format(pizza.getOrderProfit());



                String[] orderData = {customerName,
                        mobileNumber,
                        customerType,
                        locationXY,
                        distance,
                        pizzaType,
                        pizzaQuantity,
                        orderPrice,
                        orderCost,
                        orderProfit};

                orders[i] = orderData;



            }catch (CustomerException e){
                JOptionPane.showMessageDialog(PizzaGUI.this, e.getMessage());
            }catch (PizzaException e){
                JOptionPane.showMessageDialog(PizzaGUI.this, e.getMessage());
            }
        }

        JTable orderTable = new JTable(orders, columnNames);
        PizzaGUI.tableScrollPane.setViewportView(orderTable);
        PizzaGUI.getDistanceButton.setEnabled(true);
        PizzaGUI.getProfitButton.setEnabled(true);
        PizzaGUI.resetButton.setEnabled(true);
        PizzaGUI.displayLogButton.setEnabled(false);

        PizzaGUI.startupTextArea.selectAll();
        PizzaGUI.startupTextArea.replaceSelection("Log contents loaded. Calculate daily profits or\n" +
                "Calculate total delivery distance\n" +
                "Press reset to clear table and load new log.\n");
        PizzaGUI.tableScrollPane.revalidate();
        PizzaGUI.tableScrollPane.repaint();

    }

    private void displayProfit(){
        double totalProfit = restaurant.getTotalProfit();
        String dailyProfit = NumberFormat.getCurrencyInstance().format(totalProfit);
        String profitMessage = "Daily profit: " + dailyProfit;

        JOptionPane.showMessageDialog(PizzaGUI.this, profitMessage);

    }

    private void displayDistance(){
        double totalDistance = restaurant.getTotalDeliveryDistance();
        String dailyDistance = String.format("Total delivery distance traveled: %.2f blocks", totalDistance);

        JOptionPane.showMessageDialog(PizzaGUI.this, dailyDistance);

    }

    private void resetPizzaLog(){
        restaurant.resetDetails();
        restaurant = null;
        JPanel temp = new JPanel();
        PizzaGUI.tableScrollPane.setViewportView(temp);
        PizzaGUI.tableScrollPane.revalidate();
        PizzaGUI.tableScrollPane.repaint();
        PizzaGUI.startupTextArea.selectAll();
        PizzaGUI.startupTextArea.replaceSelection(STARTUP_TEXT);

        PizzaGUI.openLogButton.setEnabled(true);
        PizzaGUI.displayLogButton.setEnabled(false);
        PizzaGUI.getDistanceButton.setEnabled(false);
        PizzaGUI.getProfitButton.setEnabled(false);
        PizzaGUI.resetButton.setEnabled(false);

        //	    PizzaGUI.this.pack();
    }

    private class ButtonListener implements ActionListener{
        public void actionPerformed(ActionEvent event){
            Component source = (Component) event.getSource();
            if ( source == openLogButton ){
                importLogFile();
            }else if ( source == displayLogButton){
                displayLog();
            }else if ( source == getProfitButton){
                displayProfit();
            }else if ( source == getDistanceButton){
                displayDistance();
            }else if (source == resetButton){
                resetPizzaLog();
            }
        }
    }
}
