package asgn2Restaurant;


import java.io.BufferedReader;
import java.io.FileReader;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Scanner;

import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Pizzas.PizzaFactory;

/**
 *
 * A class that contains methods that use the information in the log file to return Pizza 
 * and Customer object - either as an individual Pizza/Customer object or as an
 * ArrayList of Pizza/Customer objects.
 * 
 * @author Roderick Lenz
 *
 */
public class LogHandler {
    private final static String COMMA = ",";
    private final static int ORDER_TIME_INDEX = 0;
    private final static int DELIVERY_TIME_INDEX = 1;
    private final static int CUSTOMER_NAME_INDEX = 2;
    private final static int MOBILE_NUMBER_INDEX = 3;
    private final static int CUSTOMER_CODE_INDEX = 4;
    private final static int X_LOCATION_INDEX = 5;
    private final static int Y_LOCATION_INDEX = 6;
    private final static int PIZZA_CODE_INDEX = 7;
    private final static int QUANTITY_INDEX = 8;

    /**
     * Returns an ArrayList of Customer objects from the information contained in the log file ordered as they appear in the log file.
     * @param filename The file name of the log file
     * @return an ArrayList of Customer objects from the information contained in the log file ordered as they appear in the log file. 
     * @throws CustomerException If the log file contains semantic errors leading that violate the customer constraints listed in Section 5.3 of the Assignment Specification or contain an invalid customer code (passed by another class).
     * @throws LogHandlerException If there was a problem with the log file not related to the semantic errors above
     * 
     */
    public static ArrayList<Customer> populateCustomerDataset(String filename) throws CustomerException, LogHandlerException{
        ArrayList<Customer> customers = new ArrayList<Customer>();
        try{
            Scanner scanner = new Scanner(filename);
            BufferedReader br = new BufferedReader(new FileReader(filename));

            String line = br.readLine();

            while(line!=null){
                customers.add(createCustomer(line));
                line = br.readLine();
            }

            br.close();
            scanner.close();

            return customers;
        }catch (LogHandlerException exception){
            //	           customers.clear();
            throw new LogHandlerException(exception.getMessage());
        }catch (CustomerException exception){
            //             customers.clear();
            throw new CustomerException(exception.getMessage());
        }catch (Exception exception){
            //	           customers.clear();
            throw new LogHandlerException("Log file error: " + exception.getMessage());
        }
    }		

    /**
     * Returns an ArrayList of Pizza objects from the information contained in the log file ordered as they appear in the log file. .
     * @param filename The file name of the log file
     * @return an ArrayList of Pizza objects from the information contained in the log file ordered as they appear in the log file. .
     * @throws PizzaException If the log file contains semantic errors leading that violate the pizza constraints listed in Section 5.3 of the Assignment Specification or contain an invalid pizza code (passed by another class).
     * @throws LogHandlerException If there was a problem with the log file not related to the semantic errors above
     * 
     */
    public static ArrayList<Pizza> populatePizzaDataset(String filename) throws PizzaException, LogHandlerException{
        ArrayList<Pizza> pizzas = new ArrayList<Pizza>();
        try{
            Scanner scanner = new Scanner(filename);
            BufferedReader br = new BufferedReader(new FileReader(filename));

            String line = br.readLine();

            while(line!=null){
                pizzas.add(createPizza(line));
                line = br.readLine();
            }

            br.close();
            scanner.close();

            return pizzas;
        }catch (LogHandlerException exception){
            //	        pizzas.clear();
            throw new LogHandlerException(exception.getMessage());
        }catch (PizzaException exception){
            //	        pizzas.clear();
            throw new PizzaException(exception.getMessage());
        }catch (Exception exception){
            //	        pizzas.clear();
            throw new LogHandlerException("Log file error: " + exception.getMessage());
        }

    }		


    /**
     * Creates a Customer object by parsing the  information contained in a single line of the log file. The format of 
     * each line is outlined in Section 5.3 of the Assignment Specification.  
     * @param line - A line from the log file
     * @return- A Customer object containing the information from the line in the log file
     * @throws CustomerException - If the log file contains semantic errors leading that violate the customer constraints listed in Section 5.3 of the Assignment Specification or contain an invalid customer code (passed by another class).
     * @throws LogHandlerException - If there was a problem parsing the line from the log file.
     */
    public static Customer createCustomer(String line) throws CustomerException, LogHandlerException{
        Customer newCustomer;
        String customerCode, name, mobileNumber;
        int locationX, locationY;

        String[] orderDetails = line.split(COMMA);
        if (orderDetails.length!= 9){
            throw new LogHandlerException("Error in log file, missing value");
        }

        try{
            customerCode = orderDetails[CUSTOMER_CODE_INDEX];
            name = orderDetails[CUSTOMER_NAME_INDEX];
            mobileNumber = orderDetails[MOBILE_NUMBER_INDEX];
            locationX = Integer.parseInt(orderDetails[X_LOCATION_INDEX]);
            locationY = Integer.parseInt(orderDetails[Y_LOCATION_INDEX]);
        }catch (Exception exception){
            throw new LogHandlerException("Log file error: " + exception.getMessage());
        }

        newCustomer = CustomerFactory.getCustomer(customerCode, name, mobileNumber, locationX, locationY);

        return newCustomer;

    }

    /**
     * Creates a Pizza object by parsing the information contained in a single line of the log file. The format of 
     * each line is outlined in Section 5.3 of the Assignment Specification.  
     * @param line - A line from the log file
     * @return- A Pizza object containing the information from the line in the log file
     * @throws PizzaException If the log file contains semantic errors leading that violate the pizza constraints listed in Section 5.3 of the Assignment Specification or contain an invalid pizza code (passed by another class).
     * @throws LogHandlerException - If there was a problem parsing the line from the log file.
     */
    public static Pizza createPizza(String line) throws PizzaException, LogHandlerException{
        Pizza newPizza;
        String pizzaType;
        int quantity;
        LocalTime orderTime, deliveryTime;

        String[] orderDetails = line.split(COMMA);
        if (orderDetails.length!= 9){
            throw new LogHandlerException("Error in log file, missing value");
        }

        try{
            pizzaType = orderDetails[PIZZA_CODE_INDEX];
            quantity = Integer.parseInt(orderDetails[QUANTITY_INDEX]);
            orderTime = LocalTime.parse(orderDetails[ORDER_TIME_INDEX]);
            deliveryTime = LocalTime.parse(orderDetails[DELIVERY_TIME_INDEX]);
        }catch (Exception exception){
            throw new LogHandlerException("Log file error: " + exception.getMessage());
        }

        newPizza = PizzaFactory.getPizza(pizzaType, quantity, orderTime, deliveryTime);
        return newPizza;

    }

}
