package asgn2Pizzas;

import java.time.LocalTime;

import asgn2Exceptions.PizzaException;


/**
 * An abstract class that represents pizzas sold at the Pizza Palace restaurant. 
 * The Pizza class is used as a base class of VegetarianPizza, MargheritaPizza and MeatLoversPizza. 
 * Each of these subclasses have a different set of toppings. A description of the class's fields
 * and their constraints is provided in Section 5.1 of the Assignment Specification. 
 * 
 * @author Roderick Lenz
 *
 */
public abstract class Pizza  {
    private static final int MAX_QUANTITY = 10, MIN_QUANTITY = 1;
    private static final LocalTime EARLIEST_ORDER_TIME = LocalTime.of(19, 0);
    private static final LocalTime LATEST_ORDER_TIME = LocalTime.of(23, 0);
    private double price, cost;
    private int quantity;
    private LocalTime orderTime, deliveryTime;
    private String pizzaType;
    protected PizzaTopping[] toppings;
    /**
     *  This class represents a pizza produced at the Pizza Palace restaurant.  A detailed description of the class's fields
     *  and parameters is provided in the Assignment Specification, in particular in Section 5.1. 
     *  A PizzaException is thrown if the any of the constraints listed in Section 5.1 of the Assignment Specification
     *  are violated. 
     *
     *  PRE: TRUE
     *  POST: All field values except cost per pizza are set
     * 
     * @param quantity - The number of pizzas ordered 
     * @param orderTime - The time that the pizza order was made and sent to the kitchen 
     * @param deliveryTime - The time that the pizza was delivered to the customer
     * @param type -  A human understandable description of this Pizza type
     * @param price - The price that the pizza is sold to the customer
     * @throws PizzaException if supplied parameters are invalid 
     * 
     */
    public Pizza(int quantity, LocalTime orderTime, LocalTime deliveryTime, String type, double price) throws PizzaException{

        if (quantity< MIN_QUANTITY || quantity > MAX_QUANTITY){
            throw new PizzaException("Invalid order quantity");
        }

        if (orderTime.isBefore(EARLIEST_ORDER_TIME)||orderTime.isAfter(LATEST_ORDER_TIME)){
            throw new PizzaException("Invalid order time");
        }

        // If Pizza is late and discarded, set price to zero to reflect loss of profit. Otherwise set price as normal
        if (deliveryTime.minusMinutes(60).isAfter(orderTime)){
            this.price = 0;
        } else {
            this.price = price;
        }

        this.quantity = quantity;
        this.orderTime = orderTime;
        this.deliveryTime = deliveryTime;
        this.pizzaType = type;
    }

    /**
     * Calculates how much a pizza would cost to make calculated from its toppings.
     *  
     * <P> PRE: TRUE
     * <P> POST: The cost field is set to sum of the Pizzas's toppings
     */
    public final void calculateCostPerPizza(){
        for (PizzaTopping pt: this.toppings){
            this.cost+=pt.getCost();
        }
    }

    /**
     * Returns the amount that an individual pizza costs to make.
     * @return The amount that an individual pizza costs to make.
     */
    public final double getCostPerPizza(){
        return this.cost;
    }

    /**
     * Returns the amount that an individual pizza is sold to the customer.
     * @return The amount that an individual pizza is sold to the customer.
     */
    public final double getPricePerPizza(){
        return this.price;
    }

    /**
     * Returns the amount that the entire order costs to make, taking into account the type and quantity of pizzas. 
     * @return The amount that the entire order costs to make, taking into account the type and quantity of pizzas. 
     */
    public final double getOrderCost(){
        double orderCost;
        orderCost = this.cost * this.quantity;
        return orderCost;
    }

    /**
     * Returns the amount that the entire order is sold to the customer, taking into account the type and quantity of pizzas. 
     * @return The amount that the entire order is sold to the customer, taking into account the type and quantity of pizzas. 
     */
    public final double getOrderPrice(){
        double orderPrice;
        orderPrice = this.price * this.quantity;
        return orderPrice;
    }


    /**
     * Returns the profit made by the restaurant on the order which is the order price minus the order cost. 
     * @return  Returns the profit made by the restaurant on the order which is the order price minus the order cost.
     */
    public final double getOrderProfit(){
        double orderProfit;
        orderProfit = this.getOrderPrice() - this.getOrderCost();
        return orderProfit;
    }


    /**
     * Indicates if the pizza contains the specified pizza topping or not. 
     * @param topping -  A topping as specified in the enumeration PizzaTopping
     * @return Returns  true if the instance of Pizza contains the specified topping and false otherwise.
     */
    public final boolean containsTopping(PizzaTopping topping){
        return false;
    }

    /**
     * Returns the quantity of pizzas ordered. 
     * @return the quantity of pizzas ordered. 
     */
    public final int getQuantity(){
        return this.quantity;
    }

    /**
     * Returns a human understandable description of the Pizza's type. 
     * The valid alternatives are listed in Section 5.1 of the Assignment Specification. 
     * @return A human understandable description of the Pizza's type.
     */
    public final String getPizzaType(){
        return this.pizzaType;
    }


    /**
     * Compares *this* Pizza object with an instance of an *other* Pizza object and returns true if  
     * if the two objects are equivalent, that is, if the values exposed by public methods are equal.
     * You do not need to test this method.
     *  
     * @return true if *this* Pizza object and the *other* Pizza object have the same values returned for 	
     * getCostPerPizza(), getOrderCost(), getOrderPrice(), getOrderProfit(), getPizzaType(), getPricePerPizza() 
     * and getQuantity().
     *   
     */
    @Override
    public boolean equals(Object other){
        Pizza otherPizza = (Pizza) other;
        return ((this.getCostPerPizza()) == (otherPizza.getCostPerPizza()) &&
                (this.getOrderCost()) == (otherPizza.getOrderCost())) &&				
                (this.getOrderPrice()) == (otherPizza.getOrderPrice()) &&
                (this.getOrderProfit()) == (otherPizza.getOrderProfit()) &&
                (this.getPizzaType() == (otherPizza.getPizzaType()) &&
                (this.getPricePerPizza()) == (otherPizza.getPricePerPizza()) &&
                (this.getQuantity()) == (otherPizza.getQuantity()));
    }


}
