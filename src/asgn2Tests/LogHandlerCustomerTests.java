package asgn2Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Restaurant.LogHandler;

/**
 * A class that tests the methods relating to the creation of Customer objects in the asgn2Restaurant.LogHander class.
 *
 * @author Roderick Lenz
 */
public class LogHandlerCustomerTests {

    @Test
    public void testLogHandler() throws CustomerException, LogHandlerException{
        Customer testCustomer = LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2");
        assertEquals(testCustomer.getName(),"Casey Jones");
        assertEquals(testCustomer.getMobileNumber(),"0123456789");
        assertTrue(testCustomer.getLocationX()==5);
        assertTrue(testCustomer.getLocationY()==5);
        assertEquals(testCustomer.getCustomerType(),"Driver Delivery");
    }

    @Test (expected = LogHandlerException.class)
    public void testBadLine() throws CustomerException, LogHandlerException{
        Customer testCustomer = LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones0123456789,DVC,5,5,PZV,2");
    }

    @Test (expected = LogHandlerException.class)
    public void testBadParse() throws CustomerException, LogHandlerException{
        Customer testCustomer = LogHandler.createCustomer("19:00:00,19:20:00,Casey Jones,0123456789,DVC,x,5,PZV,2");
    }

    @Test 
    public void testGoodFileName() throws CustomerException, LogHandlerException{
        String fileName = "./logs/20170101.txt";
        ArrayList<Customer> testCustomer = LogHandler.populateCustomerDataset(fileName);
    }

    @Test (expected = LogHandlerException.class)
    public void testBadFileName() throws CustomerException, LogHandlerException{
        ArrayList<Customer> testCustomer = LogHandler.populateCustomerDataset("Thereisnofile.csv");
    }

}
