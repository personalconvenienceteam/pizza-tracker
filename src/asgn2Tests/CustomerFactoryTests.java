package asgn2Tests;

import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory;
import asgn2Exceptions.CustomerException;

/**
 * A class the that tests the asgn2Customers.CustomerFactory class.
 * 
 * @author Roderick Lenz
 *
 */
public class CustomerFactoryTests {

    @Test
    public void testCreateCustomers() throws CustomerException{
        Customer deliveryPickUp = CustomerFactory.getCustomer("PUC", "NAME", "0123456789", 0, 0);
        Customer deliveryDriver = CustomerFactory.getCustomer("DVC", "NAME", "0123456789", 5, 5);
        Customer deliveryDrone = CustomerFactory.getCustomer("DNC", "NAME", "0123456789", 5, 5);
    }

    @Test (expected = CustomerException.class)
    public void testBadCustomerCode() throws CustomerException{
        Customer badCustomerCode = CustomerFactory.getCustomer("XYZ", "NAME", "0123456789", 0, 0);
    }

}
