package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory;
import asgn2Customers.DriverDeliveryCustomer;
import asgn2Customers.DroneDeliveryCustomer;
import asgn2Customers.PickUpCustomer;
import asgn2Exceptions.CustomerException;

/**
 * A class that tests the that tests the asgn2Customers.PickUpCustomer, asgn2Customers.DriverDeliveryCustomer,
 * asgn2Customers.DroneDeliveryCustomer classes. Note that an instance of asgn2Customers.DriverDeliveryCustomer 
 * should be used to test the functionality of the  asgn2Customers.Customer abstract class. 
 * 
 * @author Roderick Lenz
 * 
 *
 */
public class CustomerTests {

    @Test
    public void goodCustomerTests() throws CustomerException{
        PickUpCustomer testCustomerP = new PickUpCustomer("NAME", "0123456789", 0, 0 );
        DriverDeliveryCustomer testCustomerV = new DriverDeliveryCustomer("NAME", "0123456789", 1,1);
        DroneDeliveryCustomer testCustomerD = new DroneDeliveryCustomer("NAME", "0123456789", 1,1);
    }

    @Test (expected = CustomerException.class)
    public void badPickUpTest() throws CustomerException{
        PickUpCustomer testCustomerP = new PickUpCustomer("NAME", "0123456789", 1, 1);
    }

    @Test (expected = CustomerException.class)
    public void badDriverTest() throws CustomerException{
        DriverDeliveryCustomer testCustomerV = new DriverDeliveryCustomer("NAME", "0123456789", 0,0);
    }

    @Test (expected = CustomerException.class)
    public void badDriverTestTooFarEast() throws CustomerException{
        DriverDeliveryCustomer testCustomerV = new DriverDeliveryCustomer("NAME", "0123456789", 11,0);
    }

    @Test (expected = CustomerException.class)
    public void badDriverTestTooFarWest() throws CustomerException{
        DriverDeliveryCustomer testCustomerV = new DriverDeliveryCustomer("NAME", "0123456789", -11,0);
    }

    @Test (expected = CustomerException.class)
    public void badDriverTestTooFarNorth() throws CustomerException{
        DriverDeliveryCustomer testCustomerV = new DriverDeliveryCustomer("NAME", "0123456789", 0,11);
    }

    @Test (expected = CustomerException.class)
    public void badDriverTestTooFarSouth() throws CustomerException{
        DriverDeliveryCustomer testCustomerV = new DriverDeliveryCustomer("NAME", "0123456789", 0,-11);
    }


    @Test (expected = CustomerException.class)
    public void badDroneTest() throws CustomerException{
        DroneDeliveryCustomer testCustomerD = new DroneDeliveryCustomer("NAME", "0123456789", 0,0);
    }

    @Test (expected = CustomerException.class)
    public void badDroneTestTooFarEast() throws CustomerException{
        DroneDeliveryCustomer testCustomerD = new DroneDeliveryCustomer("NAME", "0123456789", 11,0);
    }

    @Test (expected = CustomerException.class) 
    public void badDroneTestTooFarWest() throws CustomerException{
        DroneDeliveryCustomer testCustomerD = new DroneDeliveryCustomer("NAME", "0123456789", -11,0);
    }

    @Test (expected = CustomerException.class)    
    public void badDroneTestTooFarNorth() throws CustomerException{
        DroneDeliveryCustomer testCustomerD = new DroneDeliveryCustomer("NAME", "0123456789", 0,11);
    }

    @Test (expected = CustomerException.class)
    public void badDroneTestTooFarSouth() throws CustomerException{
        DroneDeliveryCustomer testCustomerD = new DroneDeliveryCustomer("NAME", "0123456789", 0,-11);
    }

    @Test
    public void testCustomerAbstract() throws CustomerException{
        Customer testCustomer = CustomerFactory.getCustomer("DVC", "NAME", "0123456789", 9, 9);
        assertEquals(testCustomer.getCustomerType(), "Driver Delivery");
        assertTrue(testCustomer.getDeliveryDistance() == 18);
        assertTrue(testCustomer.getLocationX() == 9);
        assertTrue(testCustomer.getLocationY() == 9);
        assertTrue(testCustomer.getMobileNumber() == "0123456789");
        assertTrue(testCustomer.getName() == "NAME");
        assertTrue(testCustomer.equals(testCustomer));
    }

    @Test (expected = CustomerException.class)
    public void testBadCustomerName() throws CustomerException{
        Customer testCustomer = CustomerFactory.getCustomer("DVC", "", "0123456789", 9, 9);
    }

    @Test (expected = CustomerException.class)
    public void testBadCustomerSpaceName() throws CustomerException{
        Customer testCustomer = CustomerFactory.getCustomer("DVC", "         ", "0123456789", 9, 9);
    }

    @Test (expected = CustomerException.class)
    public void testBadCustomerNameLong() throws CustomerException{
        Customer testCustomer = CustomerFactory.getCustomer("DVC", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "0123456789", 9, 9);
    }

    @Test (expected = CustomerException.class)
    public void testBadCustomerNumber() throws CustomerException{
        Customer testCustomer = CustomerFactory.getCustomer("DVC", "", "1123456789", 9, 9);
    }

    @Test (expected = CustomerException.class)
    public void testBadCustomerNumberShort() throws CustomerException{
        Customer testCustomer = CustomerFactory.getCustomer("DVC", "", "012356789", 9, 9);
    }

    @Test (expected = CustomerException.class)
    public void testBadCustomerNumberNotNum() throws CustomerException{
        Customer testCustomer = CustomerFactory.getCustomer("DVC", "", "0a2356789", 9, 9);
    }

}
