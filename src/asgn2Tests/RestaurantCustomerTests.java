package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Restaurant.PizzaRestaurant;

/**
 * A class that that tests the methods relating to the handling of Customer objects in the asgn2Restaurant.PizzaRestaurant
 * class as well as processLog and resetDetails.
 * 
 * @author Roderick Lenz
 */
public class RestaurantCustomerTests {
    @Test
    public void testCreateCustomers() throws CustomerException, PizzaException, LogHandlerException{
        String fileName = "./logs/20170101.txt";
        PizzaRestaurant restaurant = new PizzaRestaurant();
        boolean logOkay = restaurant.processLog(fileName);
        assertTrue(logOkay);
        assertTrue(restaurant.getNumCustomerOrders()==3);
        assertFalse(restaurant.getCustomerByIndex(0).equals(restaurant.getCustomerByIndex(1)));
        assertTrue(restaurant.getTotalDeliveryDistance()==15);
    }
}
